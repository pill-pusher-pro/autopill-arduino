#include <Servo.h>
#include <Wire.h>
#include <DS3231.h>
#include <EEPROM.h>
#include "autopill.h"
#include "command.h"

// current dispensing state; cycles through for each pill
enum DispensingState {
    DS_IDLE = 0,
    DS_PILLCHECK,
    DS_DISPENSE,
    DS_RETURN_WAIT,
    DS_RETURN,
};

// overall dispensing status
enum DispensingResult {
    RES_UNKNOWN = 0,
    RES_INPROGRESS = 1,
    RES_COMPLETE = 2,
    RES_CANCELLED = 3,
    RES_FAIL_EMPTY = 4,
    RES_FAIL_STUCK = 5
};

// ================================================================================
// Config
// ================================================================================

static const uint32_t EEPROM_SIG = 0x0663AEB9;
static const int EEPROM_GLOBAL_BASE = 4;
static const int EEPROM_DISPENSER_BASE = 16;
static const int EEPROM_DISPENSER_SIZE = 8;

static const int SERVO_MIN_BASE = 100;
static const int SERVO_MAX_BASE = 1500;

struct GlobalConfig {
    uint8_t pin_motor[4];
    uint8_t motor_speed;
    uint8_t pillcheck_timeout;
};

struct DispenserConfig {
    // Use "invalid" flag since EEPROM defaults to 0xFF
    uint8_t invalid:1;
    uint8_t invert_enable:1;
    uint8_t spring:1;
    uint8_t sensor_digital:1;
    uint8_t reserved1:4;
    uint8_t pin_servo, pin_enable, pin_sensor;
    uint8_t sv_min, sv_max, sensor_threshold;
    uint8_t reserved2;
};

GlobalConfig config;

// ================================================================================
// Board-specific default configuration
// ================================================================================

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)

// ======================
// Arduino Mega
// ======================

static const GlobalConfig default_config PROGMEM = {
    { 2, 3, 4, 5 },
    255,
    10,
};

static const int NUM_UNITS = 10;

static const uint8_t dispensing_unit_pin_default[] PROGMEM = {
    46, 48,  0,
    22, 24,  1,
    26, 28,  2,
    30, 32,  3,
    11, 10,  4,
     9,  8,  5,
     7,  6,  6,
    14, 15,  7,
    16, 17,  8,
    18, 19,  9,
};

#elif defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)

// ======================
// Arduino Uno
// ======================

static const GlobalConfig default_config PROGMEM = {
    { 3, 5, 6, 9 },
    255,
    10
};

static const int NUM_UNITS = 4;

static const uint8_t dispensing_unit_pin_default[] PROGMEM = {
    2, 4, 0,
    7, 8, 1,
    10, 11, 2,
    12, 13, 3,
};

#endif

// ================================================================================
// Pill loading motor sequence
// ================================================================================

static const uint16_t I_RUN    = 0x4000;
static const uint16_t I_BR     = 0x8000;
static const uint16_t I_MSWAP  = 0x0100;

// I_RUN flags

static const uint16_t M_LF = I_RUN | 0x0800;
static const uint16_t M_LB = I_RUN | 0x0400;
static const uint16_t M_RF = I_RUN | 0x0200;
static const uint16_t M_RB = I_RUN | 0x0100;

static const uint16_t M_UP = M_LB | M_RF;
static const uint16_t M_DN = M_LF | M_RB;
static const uint16_t M_LEFT = M_LF | M_RF;
static const uint16_t M_RIGHT = M_LB | M_RB;

// Slow the motors for this step
static const uint16_t M_SLOW = 0x0100;

// For pincher dispensers, return the servo to neutral for this step (reduces power usage
// from servo fighting spring)
static const uint16_t M_SVN = 0x2000;

/*
Instruction formats:

  ==================
  Misc

  0 0 C C C C C C  <arg>
      `-----------  Command

  Commands:
    00: Undefined

    01: Set swap flag
        arg: 0 = toggle, 1 = set, 2 = clear

    02 .. 3F: Undefined

  ==================
  Run motor

  0 1 N S L L R R  <delay, centiseconds>
      | | |   |
      | | |   `--- Right motor command: 00 = off, 01 = back, 10 = fwd
      | | `------- Left motor command:  00 = off, 01 = back, 10 = fwd
      | ` -------- Servo to neutral
      ` ---------- Run at slower speed

  ==================
  Branch

  1 0 I C C C C C  <position>
      | `--------- Condition
      `----------- Invert condition

  Conditions:
    00: Always

    01: If current dispenser is spring-type

    03 .. 3F: Undefined
*/

static const int MAX_SEQ = 128;

uint16_t pillcheck_sequence[MAX_SEQ] = {
    10 | I_RUN,

    50 | M_UP,

    30 | M_DN,
    50 | M_UP,

    30 | M_DN,
    50 | M_UP,

    30 | M_DN,
    50 | M_UP,

    50 | M_SVN,

    30 | M_LF,
    30 | M_RB,
    30 | M_LF,
    30 | M_RB,

    50 | M_SVN,

    30 | M_LB,
    30 | M_RB,
    30 | M_LB,
    30 | M_RB,

    50 | M_SVN,

    0 | I_MSWAP,

    1 | I_BR,
};

DispenserConfig current_dispenser;
char current_dispenser_index = -1;
bool servo_attached = false;

// Set higher to enable more debug info
char info_level = 1;

DispensingState dispensing_state = DS_IDLE;
DispensingResult dispensing_result = RES_UNKNOWN;

int dispense_count = 0;
int dispense_remain = 0;
uint32_t dispensing_state_start;
uint32_t pillcheck_sequence_start;

bool alt_dir = false;
uint16_t pillcheck_timeout = 10;
uint8_t pillcheck_sequence_pos = 0;
uint8_t pillcheck_last_command = 0;
uint8_t pillcheck_sequence_command;
uint16_t pillcheck_sequence_time;

Servo servo;

bool pill_in_chamber = false;
bool pill_in_chamber_stable = false;
uint32_t pill_in_chamber_switch = 0;

DS3231 rtc;

void set_sequence_pos(uint8_t pos);
void servo_attach();
void servo_detach();
void send_dispensing_status();

// ================================================================================
// Dispenser I/O
// ================================================================================

inline void set_enable(bool value) {
    digitalWrite(current_dispenser.pin_enable, (current_dispenser.invert_enable ^ value) ? HIGH : LOW);
}

int raw_sensor_value() {
    if (current_dispenser.invalid) return 0;
    if (current_dispenser.sensor_digital) {
        return digitalRead(current_dispenser.pin_sensor) ? 1024 : 0;
    } else {
        return analogRead(current_dispenser.pin_sensor);
    }
}

bool raw_pill_in_chamber() {
    return (raw_sensor_value()) >> 2 > current_dispenser.sensor_threshold;
}

void servo_attach() {
    if (servo_attached) return;
    if (current_dispenser.invalid) return;

    int servo_min = SERVO_MIN_BASE + current_dispenser.sv_min * 10;
    int servo_max = SERVO_MAX_BASE + current_dispenser.sv_max * 10;

    servo.detach();
    servo.attach(current_dispenser.pin_servo, servo_min, servo_max);

    servo_attached = true;
}

void servo_detach() {
    if (!servo_attached) return;
    servo.detach();

    if (!current_dispenser.invalid) {
        // Leave the pin high to prevent jerking
        digitalWrite(current_dispenser.pin_servo, HIGH);
    }
    servo_attached = false;
}

// Move current servo to position to load a pill from the hopper
void servo_load() {
    servo.write(current_dispenser.spring ? 0 : 90);
}

// Move current servo to neutral position. Not applicable for slider dispensers (equivalent to servo_load)
void servo_neutral() {
    servo.write(current_dispenser.spring ? 30 : 90);
}

// Move current servo to position to dispense the pill
void servo_dispense() {
    servo.write(current_dispenser.spring ? 90 : 0);
}

// Move current servo to position to release the dispensing mechanism for maintenance
void servo_release() {
    servo.write(120);
}

void set_motor(int pinf, int pinb, int val) {
    int offpin, onpin;
    if (val < 0) {
        val = -val;
        onpin = pinb;
        offpin = pinf;
    } else {
        onpin = pinf;
        offpin = pinb;
    }
    digitalWrite(offpin, LOW);
    delayMicroseconds(1000);
    if (val == 255) {
        digitalWrite(onpin, HIGH);
    } else if (val == 0) {
        digitalWrite(onpin, LOW);
    } else {
        analogWrite(onpin, val);
    }
}

void set_motor_state(uint8_t cmd) {
    int lspeed = 0;
    int rspeed = 0;
    int mspeed = config.motor_speed;

    if (cmd & 32) {
        servo_neutral();
    } else {
        servo_load();
    }

    if (cmd & 16) {
        mspeed = mspeed * 2 / 3;
    }

    if (cmd & 8) {
        lspeed = mspeed;
    } else if (cmd & 4) {
        lspeed = -mspeed;
    }

    if (cmd & 2) {
        rspeed = mspeed;
    } else if (cmd & 1) {
        rspeed = -mspeed;
    }

    // swap left & right, reverse direction
    if (alt_dir) {
        mspeed = lspeed;
        lspeed = -rspeed;
        rspeed = -mspeed;
    }

    set_motor(config.pin_motor[0], config.pin_motor[1], lspeed);
    set_motor(config.pin_motor[2], config.pin_motor[3], rspeed);
    pillcheck_last_command = cmd;
}

// ================================================================================
// Config
// ================================================================================

void load_dispenser_config(char index) {
    if (index < 0 || index >= NUM_UNITS) {
        current_dispenser.invalid = 1;
        return;
    }

    int addr = EEPROM_DISPENSER_BASE + (index * EEPROM_DISPENSER_SIZE);
    EEPROM.get(addr, current_dispenser);

    if (current_dispenser.invalid) {
        int base = index * 3;
        current_dispenser.invalid = 0;
        current_dispenser.invert_enable = 1;
        current_dispenser.sensor_digital = 0;
        current_dispenser.spring = 0;
        current_dispenser.sensor_threshold = 128;
        current_dispenser.reserved1 = 0xF;
        current_dispenser.reserved2 = 0xFF;

        current_dispenser.pin_servo = pgm_read_byte_near(&dispensing_unit_pin_default[base + 0]);
        current_dispenser.pin_enable = pgm_read_byte_near(&dispensing_unit_pin_default[base + 1]);
        current_dispenser.pin_sensor = pgm_read_byte_near(&dispensing_unit_pin_default[base + 2]);

        current_dispenser.sv_min = ((500 - SERVO_MIN_BASE) / 10);
        current_dispenser.sv_max = ((2100 - SERVO_MAX_BASE) / 10);
    } else if (current_dispenser.pin_enable == 0xFF || current_dispenser.pin_servo == 0xFF || current_dispenser.pin_sensor == 0xFF) {
        current_dispenser.invalid = 1;
    }
    if (current_dispenser.sensor_threshold == 0xFF) {
        current_dispenser.sensor_threshold = 128;
    }
}

void save_dispenser_config(char index) {
    if (index < 0 || index >= NUM_UNITS)
        return;
    int addr = EEPROM_DISPENSER_BASE + (index * EEPROM_DISPENSER_SIZE);
    EEPROM.put(addr, current_dispenser);
}

void save_config() {
    EEPROM.put(EEPROM_GLOBAL_BASE, config);
}

void reset_config() {
    uint32_t sig = EEPROM_SIG;
    PLF(">resetting configuration");

    // copy default config
    memcpy_P(&config, &default_config, sizeof(GlobalConfig));

    // save the global config
    EEPROM.put(EEPROM_GLOBAL_BASE, config);

    // clear out the first byte of all dispenser configs
    for (int j = 0; j < NUM_UNITS; j++) {
        int addr = EEPROM_DISPENSER_BASE + (j * EEPROM_DISPENSER_SIZE);
        EEPROM.update(addr, 0xFF);
    }

    // add signature
    EEPROM.put(0, sig);
}

void setup_pins() {
    for (int j = 0; j < NUM_UNITS; j++) {
        load_dispenser_config(j);
        if (current_dispenser.invalid) continue;
        set_enable(false);
        pinMode(current_dispenser.pin_servo, INPUT);
        pinMode(current_dispenser.pin_enable, OUTPUT);
        if (current_dispenser.sensor_digital) {
            pinMode(current_dispenser.pin_sensor, INPUT);
        }
    }

    for (int j = 0; j < 4; j++) {
        digitalWrite(config.pin_motor[j], LOW);
        pinMode(config.pin_motor[j], OUTPUT);
    }
}

void setup() {
    Serial.begin(115200);

    uint32_t sig;
    EEPROM.get(0, sig);
    if (sig == EEPROM_SIG) {
        EEPROM.get(EEPROM_GLOBAL_BASE, config);
    } else {
        reset_config();
    }

    setup_pins();


    Wire.begin();

    current_dispenser_index = -1;
    current_dispenser.invalid = 1;

    command_setup();

    PLF(">autopill initialized");
}

// ================================================================================
// RTC
// ================================================================================

bool read_rtc(int& year, byte& month, byte& day, byte& hour, byte& min, byte& sec) {
    for (int i = 0; i < 20; i++) {
        bool h12, is_pm;
        bool century;

        byte sec1 = rtc.getSecond();
        byte xminute = rtc.getMinute();
        byte xhour = rtc.getHour(h12, is_pm);
        byte xday = rtc.getDate();
        byte xmonth = rtc.getMonth(century);
        byte xyear = rtc.getYear();
        byte sec2 = rtc.getSecond();
        if (sec1 == sec2) {
            year = xyear + (century ? 2100 : 2000);
            month = xmonth;
            day = xday;
            hour = xhour;
            min = xminute;
            sec = sec2;
            return true;
        }
    }
    return false;
}

void set_rtc(int year, byte month, byte day, byte hour, byte min, byte sec) {
    rtc.setClockMode(false);
    rtc.setSecond(0);
    rtc.setYear(year % 100);
    rtc.setMonth(month);
    rtc.setDate(day);
    rtc.setHour(hour);
    rtc.setMinute(min);
    rtc.setSecond(sec);
}

// ================================================================================
// Dispensing state machine
// ================================================================================

void start_pillcheck() {
    servo_load();
    dispensing_state_start = millis();
    pillcheck_sequence_start = dispensing_state_start;
    set_sequence_pos(0);
    set_motor_state(0);
    dispensing_state = DS_PILLCHECK;
    dispensing_result = RES_INPROGRESS;
    send_dispensing_status();
}

void start_dispense() {
    servo_attach();
    set_motor_state(0);
    dispensing_state_start = millis();
    servo_dispense();
    dispensing_state = DS_DISPENSE;
    send_dispensing_status();
}

void start_return_wait() {
    servo_attach();
    dispensing_state_start = millis();
    dispensing_state = DS_RETURN_WAIT;
    send_dispensing_status();
}

void start_return() {
    servo_attach();
    dispensing_state_start = millis();
    servo_neutral();
    dispensing_state = DS_RETURN;
    send_dispensing_status();
}

void set_idle() {
    if (dispensing_state != DS_IDLE) {
        set_motor_state(0);
        servo_attach();
        servo_neutral();
        dispensing_state = DS_IDLE;
        send_dispensing_status();
    }
}

void cancel_dispense() {
    dispense_remain = 0;
    dispensing_result = RES_CANCELLED;

    switch(dispensing_state) {
        case DS_PILLCHECK:
            // stop the motors
            set_motor_state(0);
            set_idle();
            break;
        case DS_DISPENSE:
            start_return_wait();
            break;
        default:
            break;
    }
}

#define NEXTI() pos++; goto retry

// Undefined instruction - just pause
#define UNDEF() warn_undef_instruction(pos); return

static void warn_undef_instruction(uint8_t pos) {
    uint16_t cmd = pillcheck_sequence[pos];

    PF(">WARN:UNDEF ");
    P(pos);
    PF(" cmd=");
    P(pillcheck_sequence_command, HEX);
    PF(" arg=");
    PL(cmd & 0xFF);

    pillcheck_sequence_pos = pos;
    pillcheck_sequence_command = M_SVN;
    pillcheck_sequence_time = 500;
}

void set_sequence_pos(uint8_t pos) {
    uint8_t ninst = 0;
    bool condition;

retry:

    if (++ninst > 30) {
        PF(">WARN:LOOP ");
        PL(pos);

        pillcheck_sequence_pos = pos;
        pillcheck_sequence_time = 500;
        return;
    }

    if (pos >= MAX_SEQ) {
        PF(">WARN:OVERRUN ");
        PL(pos);
    }

    uint16_t cmd = pillcheck_sequence[pos];

    pillcheck_sequence_command = cmd >> 8;
    if (info_level >= 3) {
        PF(">seq ");
        P(pos);
        PF(" cmd=");
        P(pillcheck_sequence_command, HEX);
        PF(" arg=");
        PL(cmd & 0xFF);
    }

    switch(pillcheck_sequence_command & 0xC0) {
        case 0x00: // Misc
            switch(pillcheck_sequence_command & 0x3F) {
                case 0x01:
                    switch(cmd & 0xFF) {
                        case 0: alt_dir = !alt_dir; break;
                        case 1: alt_dir = false; break;
                        case 2: alt_dir = true; break;
                    }
                    NEXTI();
                    break;

                default:
                    UNDEF();
            }
            break;

        case 0x40: // Run motors
            pillcheck_sequence_time = 10 * (cmd & 0xFF);
            pillcheck_sequence_pos = pos;
            return;

        case 0x80: // Branch
            condition = false;
            switch(pillcheck_sequence_command & 0x1F) {
                case 0: condition = true; break;
                case 1: condition = current_dispenser.spring; break;
            }

            if (pillcheck_sequence_command & 0x20)
                condition = !condition;

            pos = condition ? (cmd & 0xFF) : (pos + 1);
            goto retry;

        default:
            UNDEF();
    }

}

void dispense_check() {
    switch(dispensing_state) {
        case DS_IDLE:
            break;
        case DS_PILLCHECK: {
            if (pill_in_chamber_stable && pill_in_chamber) {
                start_dispense();
                return;
            }

            uint32_t ctime = millis();
            while ((ctime - pillcheck_sequence_start) >= pillcheck_sequence_time) {
                pillcheck_sequence_start += pillcheck_sequence_time;
                set_sequence_pos(pillcheck_sequence_pos + 1);
            }

            if ((ctime - dispensing_state_start) >= (pillcheck_timeout * 1000)) {
                dispensing_result = RES_FAIL_EMPTY;
                dispense_remain = 0;
                set_motor_state(0);
                set_idle();
                return;
            }

            if (pillcheck_sequence_command != pillcheck_last_command) {
                set_motor_state(pillcheck_sequence_command);
            }
        } break;
        case DS_DISPENSE: {
            uint32_t ctime = millis();
            if (pill_in_chamber_stable && !pill_in_chamber && (ctime - dispensing_state_start) >= 250) {
                dispense_count++;
                start_return_wait();
                return;
            }
            if ((ctime - dispensing_state_start) >= 3000) {
                dispensing_result = RES_FAIL_STUCK;
                dispense_remain = 0;
                start_return_wait();
                return;
            }

        } break;
        case DS_RETURN_WAIT: {
            uint32_t ctime = millis();
            if ((ctime - dispensing_state_start) >= 250) {
                start_return();
            }

        } break;
        case DS_RETURN: {
            uint32_t ctime = millis();
            if ((ctime - dispensing_state_start) >= 400) {
                if (dispense_remain > 0)
                    dispense_remain--;

                if (dispense_remain > 0) {
                    start_pillcheck();
                } else {
                    if (dispensing_result == RES_INPROGRESS)
                        dispensing_result = RES_COMPLETE;
                    set_idle();
                }
            }
        } break;
    }
}

// ================================================================================
// Serial communication / commands
// ================================================================================

void send_dispensing_status() {
    P(':');
    P((int)dispensing_result);
    P(':');
    P((int)dispensing_state);
    P(':');
    P(dispense_remain);
    P(':');
    P(dispense_count);
    P(':');
    P(pill_in_chamber | (pill_in_chamber_stable<<1));
    PL();
}

void select_unit(char newunit) {
    if (current_dispenser_index == newunit)
        return;

    servo_detach();
    set_motor_state(0);
    if (!current_dispenser.invalid) {
        // Float the servo pin, otherwise it may back-power the dispenser circuit
        pinMode(current_dispenser.pin_servo, INPUT);
        set_enable(false);
    }

    current_dispenser_index = newunit;
    load_dispenser_config(current_dispenser_index);

    if (current_dispenser.invalid) {
        current_dispenser_index = -1;
    } else {
        // Experimental result: setting servo pin high before applying
        // power prevents it from jerking
        digitalWrite(current_dispenser.pin_servo, HIGH);
        pinMode(current_dispenser.pin_servo, OUTPUT);
        set_enable(true);
        servo_attach();
        servo_neutral();

        pill_in_chamber = raw_pill_in_chamber();
        pill_in_chamber_stable = false;
        pill_in_chamber_switch = millis();
    }
}

inline void reply_message(bool res, char cmd) {
    if (cmd) {
        P(res ? '*' : '!'); P(cmd); P(' ');
    } else {
        P('/');
    }
}

bool check_dispensing(char cmd=0) {
    if (dispensing_state == DS_IDLE)
        return false;

    reply_message(false, cmd);
    PLF("dispense in progress");
    return true;
}

bool check_unit(char cmd=0) {
    if (!current_dispenser.invalid)
        return false;

    reply_message(false, cmd);
    PLF("no unit selected");
    return true;
}

template<typename T>
bool check_range(T val, T minv, T maxv, char cmd=0) {
    if (minv <= val && val <= maxv)
        return true;

    reply_message(false, cmd);
    if (val == num_limits<T>::min) {
        PLF("error: invalid value");
    } else {
        PF("error: value "); P(val); PF(" out of range ["); P(minv); PF(", "); P(maxv); PLF("]");
    }

    return false;
}

void print_dispenser_config() {
    PF("enable=");
    P(current_dispenser.pin_enable);

    PF(" servo=");
    P(current_dispenser.pin_servo);

    PF(" sensor=");
    P(current_dispenser.sensor_digital ? 'd' : 'a');
    P(current_dispenser.pin_sensor);

    PF(" spring=");
    P(current_dispenser.spring);

    PF(" invert=");
    P(current_dispenser.invert_enable);

    PF(" svmin=");
    P(SERVO_MIN_BASE + current_dispenser.sv_min * 10);

    PF(" svmax=");
    P(SERVO_MAX_BASE + current_dispenser.sv_max * 10);

    PF(" thres=");
    PL(current_dispenser.sensor_threshold);
}

bool configure_dispenser(char* cmd, int pos) {
    while (1) {
        skipspace(cmd, pos);
        char ch = cmd[pos++];
        if (!ch) break;
        switch (ch) {
            case 'i': current_dispenser.invert_enable = false; break;
            case 'I': current_dispenser.invert_enable = true; break;
            case 'p': current_dispenser.spring = false; break;
            case 'P': current_dispenser.spring = true; break;
            case 'D': current_dispenser.pin_enable = 0xFF; break;
            case 's': {
                char p = cmd[pos];
                if (p == 'a') {
                    current_dispenser.sensor_digital = false;
                    pos++;
                } else if (p == 'd') {
                    current_dispenser.sensor_digital = true;
                    pos++;
                }
                int j = rddec<int>(cmd, pos);
                if (!check_range(j, 0, 127, cmd[0]))
                    return false;
                current_dispenser.pin_sensor = j;
            } break;

            case 'v': {
                int j = rddec<int>(cmd, pos);
                if (!check_range(j, 0, 127, cmd[0]))
                    return false;
                current_dispenser.pin_servo = j;
            } break;

            case 'e': {
                int j = rddec<int>(cmd, pos);
                if (!check_range(j, 0, 127, cmd[0]))
                    return false;
                current_dispenser.pin_enable = j;
            } break;

            case 't': {
                int j = rddec<int>(cmd, pos);
                if (!check_range(j, 0, 254, cmd[0]))
                    return false;
                current_dispenser.sensor_threshold = j;
            } break;

            case 'n': {
                int j = rddec<int>(cmd, pos);
                if (!check_range(j, SERVO_MIN_BASE, SERVO_MIN_BASE + 2550, cmd[0]))
                    return false;
                current_dispenser.sv_min = ((j - SERVO_MIN_BASE) / 10);
            } break;

            case 'x': {
                int j = rddec<int>(cmd, pos);
                if (!check_range(j, SERVO_MAX_BASE, SERVO_MAX_BASE + 2550, cmd[0]))
                    return false;
                current_dispenser.sv_max = ((j - SERVO_MAX_BASE) / 10);
            } break;
            default:
                reply_message(false, cmd[0]);
                PF("invalid config char: '");
                P(ch);
                PLF("'");
                return false;
        }
    }
    return true;
}

void handle_command(char* cmd, int len) {
    int pos = 1;
    switch(cmd[0]) {
        // ================================================================================
        // Configuration commands
        // ================================================================================

        case 'i': { // set info level
            int j = rddec<int>(cmd, pos);
            if (!check_range(j, 0, 255, cmd[0]))
                return;
            info_level = j;
            reply_message(true, cmd[0]);
            PF("info_level="); PL((int)info_level);

        } break;

        case 't': { // report state
            PF("/motorspeed="); P((int)config.motor_speed); PL();
            PF("/unit="); P((int)current_dispenser_index); PL();
            PF("/timeout="); P(config.pillcheck_timeout); PL();
            PF("/info_level="); PL((int)info_level);
            PF("/pill="); P(pill_in_chamber); PL();
            PF("/stable="); P(pill_in_chamber_stable); PL();
            if (!current_dispenser.invalid) {
                int sensor_level = raw_sensor_value();
                PF("/sensor="); P(sensor_level); PL();

            }
            PF("/pin_motor=");
            for (int i = 0; i < 4; i++) {
                if (i) P(' ');
                P(config.pin_motor[i]);
            }
            PL();
            reply_message(true, cmd[0]);
            PL();
        } break;

        case 's': { // set motor speed
            int j = rddec<int>(cmd, pos);
            if (!check_range(j, 0, 255, cmd[0]))
                return;
            config.motor_speed = j;
            save_config();
            reply_message(true, cmd[0]);
            PF("motor_speed="); PL(j);
        } break;

        case 'T': { // set timeout
            int j = rddec<int>(cmd, pos);
            if (!check_range(j, 0, 12000, cmd[0]))
                return;
            config.pillcheck_timeout = j;
            save_config();
            reply_message(true, cmd[0]);
            PF("timeout="); PL(j);
        } break;

        case 'n': { // set motor pins
            int i, pins[4];
            for (i = 0; i < 4; i++) {
                pins[i] = rddec<int>(cmd, pos);
                if (!check_range(pins[i], 2, 127, cmd[0]))
                    return;
            }
            for (i = 0; i < 4; i++) {
                config.pin_motor[i] = pins[i];
            }
            save_config();
            reply_message(true, cmd[0]);
            PF("pin_motor=");
            for (i = 0; i < 4; i++) {
                if (i) P(' ');
                P(config.pin_motor[i]);
            }
            PL();
        } break;
        case 'f': { // show dispenser config
            int index = rddec<int>(cmd, pos);

            if (!check_range(index, 0, NUM_UNITS - 1, cmd[0]))
                return;

            load_dispenser_config(index);

            reply_message(true, cmd[0]);
            print_dispenser_config();

            load_dispenser_config(current_dispenser_index);
        } break;

        case 'F': { // configure dispenser
            if (check_dispensing(cmd[0])) return;

            int index = rddec<int>(cmd, pos);

            if (!check_range(index, 0, NUM_UNITS - 1, cmd[0]))
                return;

            select_unit(-1);
            load_dispenser_config(index);
            current_dispenser.invalid = 0;

            if (configure_dispenser(cmd, pos)) {
                save_dispenser_config(index);
                reply_message(true, cmd[0]);
                print_dispenser_config();
                setup_pins();
            }

            current_dispenser_index = -1;
            current_dispenser.invalid = 1;

            return;
        } break;

        case 'X':
            reset_config();
            setup_pins();
            reply_message(true, cmd[0]);
            PLF("config reset");
            return;

        // ================================================================================
        // Dispenser commands
        // ================================================================================

        case 'u':
        case 'q': {
            if (check_dispensing(cmd[0])) return;
            int j = rddec<int>(cmd, pos);
            if (!check_range(j, -1, NUM_UNITS - 1, cmd[0]))
                return;
            if (cmd[0] == 'q') {
                PLF(">about to select unit");
                delay(500);
            }

            select_unit(j);
            if (cmd[0] == 'q') {
                PLF(">select complete");
                delay(500);
            }
            reply_message(true, cmd[0]);
            P("unit="); PL(j);
            if (cmd[0] == 'q') {
                delay(1000);
                select_unit(-1);
                PLF(">deselected");
            }
        } break;

        case 'U': {
            if (check_dispensing(cmd[0])) return;
            select_unit(-1);
            reply_message(true, cmd[0]);
            PL();
        } break;

        case 'P': {
            if (check_dispensing(cmd[0])) return;
            int j = rddec<int>(cmd, pos);
            if (!check_range(j, 0, NUM_UNITS - 1, cmd[0]))
                return;
            select_unit(j);

        } // fall through
        case 'p': {
            if (check_unit(cmd[0])) return;

            int count = 1;
            skipspace(cmd, pos);
            if (cmd[pos]) {
                count = rddec<int>(cmd, pos);
                if (!check_range(count, 1, 1000, cmd[0]))
                    return;
            }

            dispense_remain += count;
            if (dispense_remain > 1000)
                dispense_remain = 1000;

            reply_message(true, cmd[0]);
            P("dispense started; count="); PL(dispense_remain);

            if (dispensing_state == DS_IDLE) {
                dispense_count = 0;
                start_pillcheck();
            }
        } break;

        case 'c': {
            if (dispensing_state == DS_IDLE) {
                reply_message(false, cmd[0]);
                PLF("no dispense in progress");
                return;
            }
            cancel_dispense();
            reply_message(true, cmd[0]);
            PLF("dispense cancelled");
        } break;


        // ================================================================================
        // Diagnostic commands
        // ================================================================================

        case 'a':
        case 'b': { // direct motor control
            int j;
            if (cmd[1] == 'f') {
                j = config.motor_speed;
            } else if (cmd[1] == 'b') {
                j = -config.motor_speed;
            } else {
                j = rddec<int>(cmd, pos);
            }
            if (check_range(j, -255, 255, cmd[0])) {
                byte pin = cmd[0] == 'b' ? 2 : 0;
                set_motor(config.pin_motor[pin], config.pin_motor[pin+1], j);

                reply_message(true, cmd[0]);
                P("motor="); PL(j);
            }
        } break;

        case 'v': { // direct servo control
            if (check_unit(cmd[0])) return;

            int j = rddec<int>(cmd, pos);
            if (check_range(j, 0, 180, cmd[0])) {
                servo.write(j);

                reply_message(true, cmd[0]);
                P("servo="); PL(j);
            }
        } break;

        case 'S': { // program sequence
            int j = rddec<int>(cmd, pos);
            if (!check_range(j, 0, MAX_SEQ - 1, cmd[0]))
                return;

            skipspace(cmd, pos);
            // clear the rest of the sequence
            if (cmd[pos] == 'E') {
                for (int i = j; i < MAX_SEQ; i++) {
                    pillcheck_sequence[i] = 0;
                }
                reply_message(true, cmd[0]);
                PL();
                return;
            }

            int xcmd = rddec<int>(cmd, pos);
            if (!check_range(xcmd, 0, 255, cmd[0]))
                return;

            int arg = rddec<int>(cmd, pos);
            if (!check_range(arg, 0, 255, cmd[0]))
                return;

            pillcheck_sequence[j] = (xcmd << 8) | arg;

            reply_message(true, cmd[0]);
            PL();
        } break;

        // ================================================================================
        // RTC
        // ================================================================================
        case 'r': {
            int year;
            byte month, day, hour, min, sec;
            if (read_rtc(year, month, day, hour, min, sec)) {
                reply_message(true, cmd[0]);
                P(year); P(' ');
                P(month); P(' ');
                P(day); P(' ');
                P(hour); P(' ');
                P(min); P(' ');
                P(sec); P(' ');
                PL();
            }
        } break;

        case 'R': {
            int year = rddec<int>(cmd, pos);
            if (!check_range<int>(year, 2000, 2199, cmd[0]))
                return;

            int month = rddec<int>(cmd, pos);
            if (!check_range<int>(month, 1, 12, cmd[0]))
                return;

            int day = rddec<int>(cmd, pos);
            if (!check_range<int>(day, 1, 31, cmd[0]))
                return;

            int hour = rddec<int>(cmd, pos);
            if (!check_range<int>(hour, 0, 23, cmd[0]))
                return;

            int min = rddec<int>(cmd, pos);
            if (!check_range<int>(min, 0, 59, cmd[0]))
                return;

            int sec = rddec<int>(cmd, pos);
            if (!check_range<int>(min, 0, 59, cmd[0]))
                return;

            set_rtc(year, month, day, hour, min, sec);
            reply_message(true, cmd[0]);
            PL();

        } break;

        default:
            reply_message(false, cmd[0]);
            PLF("invalid command");
    }
}

void loop() {
    if (!current_dispenser.invalid) {
        uint32_t ctime = millis();
        bool current_data = raw_pill_in_chamber();
        if (current_data != pill_in_chamber) {
            pill_in_chamber = current_data;
            pill_in_chamber_stable = false;
            pill_in_chamber_switch = ctime;
            send_dispensing_status();
        } else {
            if (!pill_in_chamber_stable && (ctime - pill_in_chamber_switch) >= 150) {
                pill_in_chamber_stable = true;
                send_dispensing_status();
            }
        }
    }
    dispense_check();
    command_check();
}
