/* -*- mode: c++ -*- */

#ifndef AUTOPILL_H
#define AUTOPILL_H

#include <Arduino.h>
#include <limits.h>

// because we don't have std::numeric_limits
template<typename T>
struct num_limits {
};

template<>
struct num_limits<int> {
    static const int max = INT_MAX;
    static const int min = INT_MIN;
};

template<>
struct num_limits<long> {
    static const long max = LONG_MAX;
    static const long min = LONG_MIN;
};

#define P(a, ...) Serial.print(a, ## __VA_ARGS__);
#define PL(a, ...) Serial.println(a, ## __VA_ARGS__);
#define PF(a, ...) Serial.print(F(a), ## __VA_ARGS__);
#define PLF(a, ...) Serial.println(F(a), ## __VA_ARGS__);

#endif
