/* -*- mode: c++ -*- */

#ifndef COMMAND_H
#define COMMAND_H

#include <Arduino.h>
#include "autopill.h"

static const int MAXCMD = 32;

inline void skipspace(const char* buf, int& pos) {
    while (buf[pos] == ' ') pos++;
}

inline unsigned char _rdhex(char c) {
    if ('0' <= c && c <= '9')
        return c - '0';
    if ('a' <= c && c <= 'f')
        return c - 'a' + 10;
    if ('A' <= c && c <= 'F')
        return c - 'A' + 10;
    return 0xFF;
}

inline int rdhex2(const char* buf, int& pos) {
    unsigned char c1 = _rdhex(buf[pos++]);
    if (c1 == 0xFF) return INT_MIN;
    unsigned char c2 = _rdhex(buf[pos++]);
    if (c2 == 0xFF) return INT_MIN;
    return (c1 << 4) | c2;
}

template<typename T>
T rddec(const char* buf, int& pos) {
    bool neg = false;
    int r = 0;

    skipspace(buf, pos);

    if (buf[pos] == '-') {
        neg = true;
        pos++;
    }

    while (true) {
        char c = buf[pos++];
        if ('0' <= c && c <= '9') {
            if (r > (num_limits<T>::max/10))
                return num_limits<T>::min;
            r *= 10;
            r += (c - '0');
        } else {
            return neg ? -r : r;
        }
    }

}

extern char command_buf[MAXCMD + 1];
extern bool command_active;
extern int command_pos;

void command_setup();
void command_check();

// defined by app
extern void handle_command(char* cmd, int len);

#endif
