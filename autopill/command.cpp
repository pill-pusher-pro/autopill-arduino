#include "command.h"

char command_buf[MAXCMD + 1];
bool command_active;
int command_pos;

void command_setup() {
    command_active = false;
}

void command_check() {
    while (Serial.available() > 0) {
        unsigned char k = Serial.read();
        if (k == '[') {
            command_active = true;
            command_pos = 0;
        } else if (command_active) {
            if (k == ']') {
                command_active = false;
                command_buf[command_pos] = 0;
                handle_command(command_buf, command_pos);
            } else if (command_pos < MAXCMD) {
                command_buf[command_pos++] = k;
            }
        }
    }
}
