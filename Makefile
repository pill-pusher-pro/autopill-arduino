BUILD_DATE := $(shell date +'%Y-%m-%d %H:%M:%S')
ARDUINO_BOARD := arduino:avr:mega

build/autopill.ino.bin:	autopill/autopill.h autopill/autopill.cpp autopill/command.h autopill/command.cpp
	mkdir -p build/cache
	echo '#include <Arduino.h>\nextern const char DATE_TEXT[33] PROGMEM = "BUILD_DATE=$(BUILD_DATE)\\r\\n";' > autopill/build_date.cpp
	arduino-cli compile --build-path build --build-cache-path build/cache \
		--warnings all --verbose -b $(ARDUINO_BOARD) autopill
	echo "$(BUILD_DATE)" > build/build_date.txt
