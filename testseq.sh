#!/bin/sh

uspd=255
dspd=160

exec 3>/dev/ttyACM0

mcommand() { echo "[$1]" >&3; }
setmotor() {
    mcommand "$1$2"
}
m() {
    case $1 in
        u)
            setmotor a -$uspd
            setmotor b $uspd
            ;;
        d)
            setmotor a $dspd
            setmotor b -$dspd
            ;;
        l)
            setmotor a -$uspd
            setmotor b -$uspd
            ;;
        r)
            setmotor a $uspd
            setmotor b $uspd
            ;;
        dl)
            setmotor a 0
            setmotor b -$dspd
            ;;
        dr)
            setmotor a $dspd
            setmotor b 0
            ;;
        o)
            setmotor a 0
            setmotor b 0
            ;;
    esac
    sleep $2
}


#m u 1
case $1 in
    a)
        m u 1.5
        m d .5
        m u 1.5
        m d .5
        m u 1.5
        m d .5
        ;;
    b)
        for x in 1 2; do
            m l .5
            m o .5
            m u .5
            m o .5
            m r .5
            m o .5
            m u .5
            m o .5
            #m l 1
            #m d .5
            #m o .5
            #m r 1
        done
        ;;
esac

m o 0
