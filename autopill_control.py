#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import select
import traceback
import threading
from pathlib import Path

import serial
from ktpanda import ttyutil

def process_serial(fd):
    linebuf = b''
    while True:
        r, w, e = select.select((fd,), (), (), 10)
        if fd in r:
            data = os.read(fd, 256)
            if not data:
                return
            linebuf += data
            lines = linebuf.split(b'\n')
            linebuf = lines.pop()
            for line in lines:
                line = line.decode('utf8', 'ignore').rstrip('\r')
                print(line)
                if line == '>autopill initialized':
                    os.write(fd, b'[s200][i2][u1]')

def command(ser, c):
    ser.write(f'[{c}]'.encode('utf8'))

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('port')
    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    #argcomplete.autocomplete(p, validator=lambda current_input, keyword_to_check_against: True)
    args = p.parse_args()

    ser = serial.Serial(args.port, baudrate=115200)
    sfd = ser.fileno()
    thr = threading.Thread(target=process_serial, args=(sfd,), daemon=True)
    thr.start()

    arg = ''

    with ttyutil.TTYInput(signals=True) as tty:
        while True:
            ch = tty.readkey()
            isnum = False
            if ch in {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-'}:
                arg += ch
                sys.stdout.write(f'\r\033[0Karg={arg}')
                isnum = True
            elif arg:
                print()


            if ch in {'i', 't', 'a', 'b', 'v', 's', 'm', 'u', 'U', 'p', 'c'}:
                command(ser, f'{ch}{arg}')
            elif ch == ',':
                command(ser, f'v180')
            elif ch == '.':
                command(ser, f'v90')
            elif ch == '/':
                command(ser, f'v0')
            elif ch == '[':
                command(ser, 'ab')
            elif ch == ']':
                command(ser, 'af')
            elif ch == '{':
                command(ser, 'bb')
            elif ch == '}':
                command(ser, 'bf')
            elif ch == '\x1b[A':
                command(ser, 'ab')
                command(ser, 'bf')
            elif ch == '\x1b[B':
                command(ser, 'af')
                command(ser, 'bb')
            elif ch == '\x1b[C':
                command(ser, 'af')
                command(ser, 'bf')
            elif ch == '\x1b[D':
                command(ser, 'ab')
                command(ser, 'bb')
            elif ch == '\n':
                command(ser, 'a0')
                command(ser, 'b0')
            elif ch == ' ':
                command(ser, 'p')
            elif ch == 'q':
                return
            elif not isnum:
                print(f'unknown: {ch!r}')
            if not isnum:
                arg = ''


if __name__ == '__main__':
    main()


import serial
